# ORDS ReproHack 2021

My contribution to the ORDS ReproHack which took place on 2021-05-11, organised by the
University of Rostock ORDS Team. Thanks again for the organization!

## What's that about?

We came together to evaluate the publication

Luis M. Vilches-Blázquez & Daniela Ballari (2020):
**Unveiling the diversity of spatial data infrastructures in Latin America: evidence from an exploratory inquiry**,
Cartography and Geographic Information Science, DOI: [10.1080/15230406.2020.1772113](https://doi.org/10.1080/15230406.2020.1772113)

for reproducibility. For that reason we started re-implementing the data analysis carried out in the paper using Python. This *ad-hoc* approach is done in the Python script [`./main.py`](./main.py).

## Prerequisites

To run the code, install the requirements specified in the [requirements.txt](./requirements.txt) file using `pip` or `conda`. With `pip` you might need to install the packages `geos-devel` (or `geos-dev`) and `proj-devel` (`proj-dev`) from your \*nix package manager.

## Licensing

Code by myself is licensed to the MIT license.

The required shape files can be downloaded from [http://tapiquen-sig.jimdofree.com](http://tapiquen-sig.jimdofree.com). Carlos Efraín Porto Tapiquén. Geografía, SIG y Cartografía Digital. Valencia, Spain, 2020.
