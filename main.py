"""
S.D.G

"""

from pathlib import Path
from zipfile import BadZipFile

import pandas as pd
import geopandas as gpd
import geoplot
import matplotlib.pyplot as plt
import matplotlib as mpl

def dl_and_unzip(url, folder, zip_or_rar="zip"):
    import requests

    datafolder = Path(folder).resolve()
    datafolder.mkdir(exist_ok=True)
    archive = datafolder / "tmp.archive"

    response = requests.get(url, allow_redirects=True)

    # Write the URL contents to the zipfile
    with open(archive, "wb") as f:
        f.write(response.content)

    if zip_or_rar.lower() == "zip":
        import zipfile
        with zipfile.ZipFile(archive, 'r') as zip:
            zip.extractall(datafolder)

    elif zip_or_rar.lower() == "rar":
        import rarfile
        with rarfile.RarFile(archive, 'r') as rar:
            rar.extractall(datafolder)
    else:
        raise ValueError("Only 'zip' and 'rar' allowed.")

    # Remove the ZIP file, as it's not required anymore.
    archive.unlink()

try:
    plt.style.use("/scratch/projects/qdplot/custom.mplstyle")
except FileNotFoundError:
    pass

# Make the output directory
opath = Path("./output/")
if not opath.exists():
    opath.mkdir()

# Read the data
dfile = Path("data/LatinAmerica_SDI_surveys_2014_2017_2019.xls").resolve()

if not dfile.exists():
    url = "https://ndownloader.figshare.com/files/22720802"
    datafolder = Path("./data")
    dl_and_unzip(url, datafolder)

    pass

df14 = pd.read_excel(dfile, sheet_name="s.14")
df17 = pd.read_excel(dfile, sheet_name="s.17")
df19 = pd.read_excel(dfile, sheet_name="s.19")

# Make responses
responses = pd.DataFrame(
    data={
        "responses": [df14.shape[0], df17.shape[0], df19.shape[0]],
    },
    index=[2014, 2017, 2019],
)

rfig, rax = plt.subplots()
responses.plot.pie(ax=rax, subplots=False, y="responses")
rfig.savefig(opath / "responses.svg")

# Percentage plot
percentages = {
    "2014": df14["institution.type"].value_counts(normalize=True) * 100,
    "2017": df17["institution.type"].value_counts(normalize=True) * 100,
    "2019": df19["institution.type"].value_counts(normalize=True) * 100,
}

perc = pd.concat(percentages, axis=1)

pfig, pax = plt.subplots(figsize=(6,6))
perc.plot.bar(ax=pax, color=["#B9B9B9", "#FFCC99", "#3399FF"])

for p in pax.patches:
    pax.annotate(
        "{:.0f} %".format(p.get_height()),
        (p.get_x() + 0.02, p.get_height() + 1.5),
        ha="left", va="center", fontsize="small"
    )

pfig.tight_layout()
pfig.savefig(opath / "barplots.svg")

# Map plots!
dfs = {
    "2014": df14["country"].value_counts(normalize=True) * 100,
    "2017": df17["country"].value_counts(normalize=True) * 100,
    "2019": df19["country"].value_counts(normalize=True) * 100,
}
df = pd.concat(dfs, axis=1)
df.index.name = "COUNTRY"

shpfile = Path("shapes/Americas.shp").resolve()
if not shpfile.exists():
    url = "https://tapiquen-sig.jimdofree.com/app/download/5497292959/Americas.rar"
    dl_and_unzip(url, Path("shapes"), zip_or_rar="rar")

map_df = gpd.read_file(shpfile, index="COUNTRY")
map = map_df.set_index("COUNTRY")

mfig, maxs = plt.subplots(ncols=3)
merged = pd.merge(left=map, right=df, left_index=True, right_index=True)

# Do the actual plotting
for ax, year in zip(maxs, dfs.keys()):
    geoplot.choropleth(merged, ax=ax, hue=df[year], cmap="Reds")
    ax.set_title(year)

mfig.savefig(opath / "mapplots.svg")

plt.show()
